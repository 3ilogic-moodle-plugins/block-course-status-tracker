<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course completion criteria named 'grade' based on login user.
 *
 * @package    block_course_status_tracker
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
defined('MOODLE_INTERNAL') || die();

require_once("{$CFG->libdir}/formslib.php");
require_once("lib.php");

/**
 * Display list of enrolled courses based on login user.
 *
 * @copyright 3i Logic<lms@3ilogic.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class course_status_form extends moodleform {

    public function definition() {
        return false;
    }

    public function display_report() {
        global $DB, $OUTPUT, $CFG, $USER;
        $userid = $USER->id;
        // Page parameters.
        $page = optional_param('page', 0, PARAM_INT);
        $perpage = optional_param('perpage', 30, PARAM_INT);    // How many record per page.
        $sort = optional_param('sort', 'firstname', PARAM_ALPHA);
        $dir = optional_param('dir', 'DESC', PARAM_ALPHA);
       //$sql = "SELECT course, gradefinal, timecompleted as dates FROM {course_completion_crit_compl} where userid = " . $userid;
       
        $table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('module', 'block_course_status_tracker') ,get_string('course_name', 'block_course_status_tracker'), get_string('course_comp_date', 'block_course_status_tracker'), get_string('grade', 'block_course_status_tracker'), get_string('certificate', 'block_course_status_tracker'), get_string('detail', 'block_course_status_tracker'));
        $table->align = array('center', 'left', 'center', 'center','left', 'center', 'center');
        $table->data = array();
        $i = 0;
        $courses = enrol_get_users_courses($userid, false, 'id, shortname, showgrades');
        $complete = copmletion($courses);
        
	if (!empty($complete)){
        foreach ($complete as $log) {
            $row = array();
            $row[] = ++$i;
            $row[] = module_name(get_category_id($log->id));
            $row[] = "<a href=" . $CFG->wwwroot . "/course/view.php?id=" . $log->id . ">" .course_name($log->id) . "</a>";
            $row[] = userdate(dates($log->id , $userid), get_string('strftimedate', 'core_langconfig'));
            $row[] = round(grade($log->id , $userid) ) . '%';
            if (get_course_module_id($log->id) != NULL){
            $row[] = '<a href="'.$CFG->wwwroot.'/mod/iomadcertificate/view.php?id='.get_course_module_id($log->id).'"><img src="'.$CFG->wwwroot.'/blocks/course_status_tracker/pix/icon.gif" ></img></a>';
            }          
            else{
            $row[] = '-';
            }
            $row[] = '<a href="'.$CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=5&cid='.$log->id.' "><img src="'.$CFG->wwwroot.'/blocks/course_status_tracker/pix/detail.png" ></img></a>';

            //$row[] = get_user_certificate($log->course);
            $table->data[] = $row;
            
            }
}

    else{
          $table->data[] = array('', '','',get_string('no_data', 'block_course_status_tracker'),  '', '','' );
}
        return $table;
    }

}

/**
 *  Accomplishment Form
 **/
class accomplishment_form extends moodleform {

    public function definition() {
        return false;
    }

    public function display_report() {
        global $DB, $OUTPUT, $CFG, $USER;
        $userid = $USER->id;
        // Page parameters.
        $page = optional_param('page', 0, PARAM_INT);
        $perpage = optional_param('perpage', 30, PARAM_INT);    // How many record per page.
        $sort = optional_param('sort', 'firstname', PARAM_ALPHA);
        $dir = optional_param('dir', 'DESC', PARAM_ALPHA);
        $sql = "SELECT b.id as bid, b.name as name ,bi.uniquehash as hash, bi.dateissued, b.courseid as course, bcri.criteriatype as type  FROM {badge} as b  ,{badge_issued} as bi , {badge_criteria} as bcri where bi.userid = " . $userid . " AND b.id = bi.badgeid AND b.id = bcri.badgeid AND bi.visible = 1 AND bcri.criteriatype != 0"; ;
        $sql1 = "SELECT cst.id,  CONCAT(u.email, '_'  , cst.t_name,'.pdf') as certificate, cst.t_name as training , cst.method as method, cst.u_name as name, cst.issuedate as date FROM {course_status_tracker} as cst , {user} as u WHERE cst.u_name = " . $userid . " AND u.id = " . $userid . " ";
        
        
        $table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('title', 'block_course_status_tracker') , get_string('training_method', 'block_course_status_tracker') ,get_string('issued_date', 'block_course_status_tracker'), get_string('certificate', 'block_course_status_tracker'));
        $table->align = array('center', 'left', 'center', 'center','center');
        $table->data = array();
        //$orderby = "$sort $dir";
        $rs = $DB->get_records_sql($sql, array(), $page * $perpage, $perpage);
        $rs1 = $DB->get_records_sql($sql1, array(), $page * $perpage, $perpage);
        $i = 0;
          /*** For Online courses ***/ 

	if(!empty($rs) ||  !empty($rs1)){
        foreach ($rs as $log ) {
            $row = array();
            $row[] = ++$i;
            $row[] = '<a href="'.$CFG->wwwroot.'/badges/badge.php?hash='.$log->hash.'">'.$log->name.'</a>';

            $row[] = get_string('online', 'block_course_status_tracker');

            $row[] = userdate($log->dateissued, get_string('strftimedate', 'core_langconfig'));
          
            if (get_course_module_id($log->course) != NULL){
            $row[] = '<a href="'.$CFG->wwwroot.'/mod/iomadcertificate/view.php?id='.get_course_module_id($log->course).'"><img src="'.$CFG->wwwroot.'/blocks/course_status_tracker/pix/icon.gif" ></img></a>';
            }
            
            else
            {
                $row[] = '-';
            }

            $table->data[] = $row;
        }
        
        
      /*** For Offline Training ***/  
    foreach ($rs1 as $log1 ) {
        $row1 = array();
        $row1[] = ++$i;
        $row1[] = $log1->training;
        global $DB;
        if ($log1->method == 0){
         $row1[] = get_string('face_to_face', 'block_course_status_tracker');
        }
        else if($log1->method == 1){
         $row1[] = get_string('blended', 'block_course_status_tracker');   
        }
        else if($log1->method == 2){
         $row1[] = get_string('online_lms', 'block_course_status_tracker');   
        }

        $row1[] = userdate($log1->date, get_string('strftimedate', 'core_langconfig'));
        $sql = "SELECT * FROM {files} as f WHERE f.filename = '$log1->certificate'";
        if( $DB->record_exists_sql($sql, array()) == 1 ){
        $row1[] = "<a target='_new' href='" . $CFG->wwwroot . "/pluginfile.php/".get_context_id()."/mod_folder/content/0/" . strtolower($log1->certificate) . "'/><img src='".$CFG->wwwroot."/blocks/course_status_tracker/pix/icon.gif' ></img></a>";
        }
        
        else{
           $row1[] = '-'; 
        }
     
       // $row1[] = $log1->certificate;
        $table->data[] = $row1;
    }

	}

    else{
        $table->data[] = array('', '',get_string('no_data', 'block_course_status_tracker'),  '', '', '');
    }   
        return $table;
    }
}
/**
 *  Offline Training
 * 
 * 
 * 
 **/
class offline_training extends moodleform {

    public function definition() {
        global $DB, $CFG , $page;
        $mform = & $this->_form;
        $attributes = array('size' => '50', 'maxlength' => '1000');
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT); 
        
        $mform->addElement('text', 't_name', get_string('training_name', 'block_course_status_tracker'), $attributes);
        $mform->setType('t_name', PARAM_TEXT);
        $mform->addRule('t_name', get_string('validation_training', 'block_course_status_tracker'), 'required', null, 'server');
        
        $attributes = array(get_string('face_to_face', 'block_course_status_tracker'), get_string('blended', 'block_course_status_tracker'),get_string('online_lms', 'block_course_status_tracker'));
        $mform->addElement('select', 'method', get_string('training_method', 'block_course_status_tracker'), $attributes);
        
        /* New Fields */
        $mform->addElement('text', 't_body', get_string('training_body', 'block_course_status_tracker'), $attributes);
        $mform->setType('t_body', PARAM_TEXT);
        $mform->addRule('t_body', get_string('validation_training_body', 'block_course_status_tracker'), 'required', null, 'server');        
        $mform->addElement('duration', 'n_of_hours', get_string('certification_duration', 'block_course_status_tracker'),array('optional' => false));
        $mform->setType('n_of_hours', PARAM_TEXT);
        $attributes = array(get_string('yes', 'block_course_status_tracker'), get_string('no', 'block_course_status_tracker'));
        $mform->addElement('select', 'ivass', get_string('ivass', 'block_course_status_tracker'), $attributes);
        /* End New Fields */
        $attributes = $DB->get_records_sql_menu('SELECT id, CONCAT(firstname, " "  , lastname, " (", email, ") ") AS name  FROM {user} order by name', null, $limitfrom = 0, $limitnum = 0);
        $select = $mform->addElement('select', 'u_name', get_string('name', 'block_course_status_tracker'), $attributes, null, null);
        $select->setMultiple(true);        
        $mform->addRule('u_name', get_string('validation_name', 'block_course_status_tracker'), 'required', null, 'server');
        $mform->addElement('date_selector', 'issuedate', get_string('issued_date', 'block_course_status_tracker'));
        $mform->addElement('hidden', 'viewpage');
        $mform->setType('viewpage', PARAM_INT);
        $this->add_action_buttons(true, get_string('savechanges')); 
    }

    public function display_report() {
       return false;
    }

}


// Add instruction
class add_instruction extends moodleform {

    public function definition() {
        global $DB, $CFG , $page;
        $mform = & $this->_form;
        $attributes = array('size' => '50', 'maxlength' => '1000');
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $attributes = $DB->get_records_sql_menu('SELECT  id, CONCAT(firstname, " "  , lastname, " (", email, ") ") AS name  FROM {user} WHERE id NOT IN (SELECT u_name FROM {block_course_status_tracker})', null, $limitfrom = 0, $limitnum = 0);
        $mform->addElement('select', 'u_name', get_string('name', 'block_course_status_tracker'), $attributes, null, null);
        $mform->addRule('u_name', get_string('validation_name', 'block_course_status_tracker'), 'required', null, 'server');
        $mform->addElement('textarea', 'instruction', get_string("instruction", "block_course_status_tracker"), 'wrap="virtual" rows="6" cols="42"');
        $mform->setType('instruction', PARAM_TEXT);
        $mform->addRule('instruction', get_string('validation_instruction', 'block_course_status_tracker'), 'required', null, 'server');
        $mform->addElement('hidden', 'viewpage');
        $mform->setType('viewpage', PARAM_INT);
        $this->add_action_buttons($cancel = false);
    }

    public function validation($data, $files) {
        return array();
    }

    public function display_list() {
        global $DB, $OUTPUT, $CFG;
         
        
        $sql = "SELECT * FROM {block_course_status_tracker}";
        $rs = $DB->get_recordset_sql($sql, array());
        $table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('name', 'block_course_status_tracker'), get_string('instruction', 'block_course_status_tracker'),get_string('edit', 'block_course_status_tracker'), get_string('remove', 'block_course_status_tracker') );
        $table->size = array('10%', '30%', '55%', '5%');
        $table->align = array('center', 'left', 'left');
        $table->data = array();
        $i = 0;
        if ($DB->record_exists_sql($sql, array())) {
        foreach ($rs as $instruct) {
            $row = array();
            $row[] = ++$i;
            $row[] = get_user($instruct->u_name);
            $row[] = $instruct->instruction;
            $row[] = '<center><center><a title="' . get_string('edit') . '" href="' . $CFG->wwwroot . '/blocks/course_status_tracker/view.php?viewpage=10&edit=edit&id=' . $instruct->id. '"/>
                      <img alt="" src="' . $OUTPUT->pix_url('t/edit') . '" class="iconsmall" /></a></center>';
            $row[] = '<center><center><a title="' . get_string('delete') . '" href="' . $CFG->wwwroot . '/blocks/course_status_tracker/view.php?viewpage=10&rem=remove&id=' . $instruct->id. '"/>
                       <img alt="" src="' . $OUTPUT->pix_url('t/delete') . '" class="iconsmall"/></a></center>';
            $table->data[] = $row;
        }
        
        }
        else{
            $table->data[] = array('', '',get_string('no_data', 'block_course_status_tracker'),  '');
        }
    
    return $table;
    }

}


//Modify offline training List

class offline_training_list extends moodleform {

    public function definition() {
        global $DB, $CFG , $page;
        $mform = & $this->_form;
        
        $attributes = array('size' => '50', 'maxlength' => '1000');
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('text', 't_name', get_string('training_name', 'block_course_status_tracker'), $attributes);
        $mform->setType('t_name', PARAM_TEXT);
        $mform->addRule('t_name', get_string('validation_training', 'block_course_status_tracker'), 'required', null, 'server');
        //print_r($mform->t_name);
        // $attributes = array('size' => '50', 'maxlength' => '1000');
        $attributes = array(get_string('face_to_face', 'block_course_status_tracker'), get_string('blended', 'block_course_status_tracker'),get_string('online_lms', 'block_course_status_tracker'));
        $mform->addElement('select', 'method', get_string('training_method', 'block_course_status_tracker'), $attributes);
        /* New Fields */
        $mform->addElement('text', 't_body', get_string('training_body', 'block_course_status_tracker'), $attributes);
        $mform->setType('t_body', PARAM_TEXT);
        $mform->addRule('t_body', get_string('validation_training_body', 'block_course_status_tracker'), 'required', null, 'server');
        
        $mform->addElement('duration', 'n_of_hours', get_string('certification_duration', 'block_course_status_tracker'),array('optional' => false) );
        $mform->setType('n_of_hours', PARAM_TEXT);
        
        $attributes = array(get_string('yes', 'block_course_status_tracker'), get_string('no', 'block_course_status_tracker'));
        $mform->addElement('select', 'ivass', get_string('ivass', 'block_course_status_tracker'), $attributes);
        
        /* End New Fields */
        $attributes = $DB->get_records_sql_menu('SELECT id, CONCAT(firstname, " "  , lastname, " (", email, ")") AS name  FROM {user} order by name', null, $limitfrom = 0, $limitnum = 0);
        $mform->addElement('select', 'u_name', get_string('name', 'block_course_status_tracker'), $attributes, null, null);
       // $select->setMultiple(true);
        
        $mform->addElement('date_selector', 'issuedate', get_string('issued_date', 'block_course_status_tracker'));
       // $attachmentoptions = array('subdirs' => 0, 'areamaxbytes' => 10485760, 'accepted_types' => array('document'));
        $mform->addElement('hidden', 'viewpage');
        $mform->setDefault('viewpage', 9);
        $mform->setType('viewpage', PARAM_INT);
//        $mform->addElement('hidden', 'viewpage');
//        $mform->setType('viewpage', PARAM_INT);
       
//   
          //$this->add_action_buttons(true, get_string('savechanges')); 

          $this->add_action_buttons($cancel = false);
    // $this->add_action_buttons(false); 
    }

    public function validation($data, $files) {
        return array();
    }

    public function display_list() {
        return false;
        
    }

}


