<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course completion criteria named 'grade' based on login user.
 *
 * @package    block_course_status_tracker
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
global $DB, $OUTPUT, $PAGE, $CFG, $USER;
require_once('../../config.php');
require_once('course_form.php');
require_once('lib.php');
require_once($CFG->dirroot.'/lib/completionlib.php');
require_login();

$context = context_system::instance();
$viewpage = required_param('viewpage', PARAM_INT);
/****...$viewpage = optional_param('viewpage', 7, PARAM_INT); ***/
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string("pluginname", 'block_course_status_tracker'));
$PAGE->set_heading('Course Status');
$pageurl = '/blocks/course_status_tracker/view.php?viewpage=' . $viewpage;
$PAGE->set_url($pageurl);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string("pluginname", 'block_course_status_tracker'));
$rem = optional_param('rem', null, PARAM_RAW);
$edit = optional_param('edit', null, PARAM_RAW);
$delete = optional_param('delete', null, PARAM_RAW);
$id = optional_param('id', null, PARAM_INT);
echo $OUTPUT->header();
?>
<!-- Inline CSS for Back Button -->
<style>
.button {
  background-color: #036;
  border: none;
  color: white;
  padding: 6px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}



.info {
  margin-bottom: 10px;
  padding: 20px 12px;
  background-color: #e7f3fe;
  border-left: 6px solid #2196F3;
}
</style>

<!-- DataTables code starts-->
<!--<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot ?>/blocks/course_status_tracker/public/datatable/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot ?>/blocks/course_status_tracker/public/datatable/dataTables.tableTools.css">
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot ?>/blocks/course_status_tracker/public/datatable/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot ?>/blocks/course_status_tracker/public/datatable/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot ?>/blocks/course_status_tracker/public/datatable/dataTables.tableTools.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot ?>/blocks/course_status_tracker/public/datatable/dataTables.tableTools.js"></script>

-->

<!-- New Links -->
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>



<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

<script type="text/javascript" language="javascript" class="init">
    
  $(document).ready(function() {
    $('.display').DataTable( {
        dom: 'Bfrtip',
        
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        
        "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Italian.json",
            }
    } );
} );
</script>

<?php

/* Admin Pages */
//if(is_siteadmin()){
if(user_has_role_assignment($USER->id,5) != 1){
if ($viewpage == 1) {
    $form = new course_status_form();
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    $table = $form->display_report();
    if ($table) {
        echo "<div id='prints'>";
        // $title = '<center><table width="80%" style="background-color:#F3F3F3;"><tr><td><center><h2>' . get_string('report_coursecompletion', 'block_course_status_tracker') . '</h2></center></td></tr></tr><table></center>';
        $title = '<h2>' . get_string('report_coursecompletion', 'block_course_status_tracker') . '</h2>';
        $title.=user_details($USER->id);
        $a = html_writer::table($table);
        echo $title;
        echo "<br/>".$a;
        echo "</div>";
    }
} else if ($viewpage == 2) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    // $title = '<center><table width="80%" style="background-color:#F3F3F3;"><tr><td><center><h2>' . get_string('report_courseenrollment', 'block_course_status_tracker') . '</h2></center></td></tr></tr><table></center>';
    $title = '<h2>' . get_string('report_courseenrollment', 'block_course_status_tracker') . '</h2>';
    $title.=user_details($USER->id);
    echo $title;
    $a = html_writer::table(user_enrolled_courses_report($USER->id));
    echo "<br/>".$a;
    echo "</div>";
} else if ($viewpage == 3) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    $title = '<h2>' . get_string('report_courseundefined', 'block_course_status_tracker') . '</h2>';
    $title.=user_details($USER->id);
    echo $title;
	echo "<br/>".html_writer::table(user_undefined_courses_report($USER->id));
    echo "</div>";
} else if ($viewpage == 4) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    $title = '<h2>' . get_string('report_courseinprogress', 'block_course_status_tracker') . '</h2>';
    $title.=user_details($USER->id);
        echo $title;
	echo "<br/>".html_writer::table(user_inprogress_courses_report($USER->id));
    echo "</div>";
} 

// Course Details
else if ($viewpage == 5) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=1">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
        echo "<div id='prints'>";
        $title = '<h2>' . get_string('course_details', 'block_course_status_tracker') . '</h2>';
        echo $title;
       // echo user_has_role_assignment($USER->id,5);
        course_detail($USER->id);
        echo "</div>";
}

// Accomplishment 
else if ($viewpage == 6) {
    $form = new accomplishment_form();
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    $table = $form->display_report();
    if ($table) {
        echo "<div id='prints'>";
        $title = '<h2>' . get_string('accomplishment', 'block_course_status_tracker') . '</h2>';
        $title.=user_details($USER->id);
        $a = html_writer::table($table);
        echo $title;
        echo '<div class="info">';
        if (user_instruction($USER->id) !=FALSE){
        echo '<p><strong>' .get_string('instruction', 'block_course_status_tracker') .': </strong>'. user_instruction($USER->id) .'</p>';}
        else{echo '<p><strong>' .get_string('instruction', 'block_course_status_tracker') .': </strong> ' .get_string('no_instruction', 'block_course_status_tracker') .'</p>';}
         
        echo '</div>';
        echo "<br/>".$a;
        echo "</div>";
        
    }
}

// Add offline Training
else if ($viewpage == 7) {
    ?>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script>
        $(document).ready(function() {
            $("#id_u_name").select2();
        });
    </script>
        
        <?php
    $form = new offline_training();
    
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    $toform['viewpage'] = $viewpage;
    $form->set_data($toform);
        global $USER, $OUTPUT, $DB;
        $title = '<h2>' . get_string('offline_training', 'block_course_status_tracker') .'</h2>';
        
            $record = new stdClass();
            $record2 = new stdClass();
           
            
           /****  Multiple Insert ****/ 
            if ($fromform = $form->get_data()) {
             foreach ($fromform->u_name as $formtid){
                    $record->t_name = $fromform->t_name;
                    $record->method = $fromform->method;
                    $record->t_body = $fromform->t_body;
                    $record->n_of_hours = $fromform->n_of_hours;
                    $record->ivass = $fromform->ivass;
                    $record->u_name = $formtid;
                    $record->issuedate = $fromform->issuedate;
                    $DB->insert_record('course_status_tracker', $record);
                }
                redirect($CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=8');
        }
        // Form Cancel.
    if ($fromform = $form->is_cancelled()) {
        redirect($CFG->wwwroot);
    }

      echo $title;
      echo "</div>";
      $form->display();
}
//Add Instruction
else if ($viewpage == 10) {
    ?>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script>
        $(document).ready(function() {
            $("#id_u_name").select2();
        });
    </script>
        
        <?php
    $form = new add_instruction();
    
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>'; 
    
    echo "<div id='prints'>";
    $toform['viewpage'] = $viewpage;
    $form->set_data($toform);
        global $USER, $OUTPUT, $DB;
        $title = '<h2>' . get_string('add_instruction', 'block_course_status_tracker') .'</h2>';
        $record = new stdClass();
           /****  Multiple Insert ****/ 
        if ($fromform = $form->get_data()) {
            //echo $fromform->viewpage;
                if ($fromform->id) {
                    global $DB;
                    // Insert or Update data - Save button Click.
                    $DB->update_record('block_course_status_tracker', $fromform);
                    redirect($CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=10');
            }
                else{
                    $DB->insert_record('block_course_status_tracker', $fromform);           
                    redirect($CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=10');
            }
        }
    
    if ($rem) {
       echo $OUTPUT->confirm('Do you want to delete instruction?', '/blocks/course_status_tracker/view.php?viewpage=10&rem=rem&delete=' . $id, '/blocks/course_status_tracker/view.php?viewpage=10');
       if($delete){
         $sql =  $DB->delete_records('block_course_status_tracker', array ('id' => $delete)) ;
         redirect($CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=10');
       }
    }
    if ($edit) {
        $add_instruction = $DB->get_record('block_course_status_tracker', array('id' => $id), '*');
        $toform['viewpage'] = $viewpage;
        $form = new add_instruction(null, array('id' => $add_instruction->id));
        $form->set_data($toform);
        $form->set_data($add_instruction);
    }
      echo $title;
      echo "</div>";
      $form->display();
      $table = $form->display_list();
      echo html_writer::table($table);
              
    
}

    // Total offline Training
else if ($viewpage == 8) {
    global $OUTPUT;
    ?>
    <!-- Back Button-->
    <input type="button" class="button" value="Back" onclick="history.back(-1)">
    <?php
    
    echo "<div id='prints'>";
    $title = '<h2>' . get_string('offline_training', 'block_course_status_tracker') .'</h2>';
    echo $title;
    $a = html_writer::table(user_offline_training_report());
    echo "<br/>".$a;
    echo "</div>";
}

// Edit Del offline training
else if ($viewpage == 9) {
      global $USER, $OUTPUT, $DB;
       ?>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script>
        $(document).ready(function() {
            $("#id_u_name").select2();
        });
    </script>
    <?php
    $form = new offline_training_list();
    ?>
    <!-- Back Button-->
    <input type="button" class="button" value="Back" onclick="history.back(-1)">
    <?php
    
        if ($fromform = $form->get_data()) {
            if ($fromform->id) {
                global $DB;
            $DB->update_record('course_status_tracker', $fromform);
            redirect($CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=8');
          } 
        }
      
       
    if ($rem) {
       echo $OUTPUT->confirm('Do you want to delete training?', '/blocks/course_status_tracker/view.php?viewpage=9&rem=rem&delete=' . $id, '/blocks/course_status_tracker/view.php?viewpage=8');
       if($delete){
         $sql =  $DB->delete_records('course_status_tracker', array ('id' => $delete)) ;
         redirect($CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=8');
       }
       
    }
    // Edit Record.
   if ($edit) { 
        $get_offlinetraining = $DB->get_record('course_status_tracker', array('id' => $id), '*');
        $form = new offline_training_list(null, array('id' => $get_offlinetraining->id));
        $form->set_data($get_offlinetraining);
        echo "<div id='prints'>";
        $title = '<h2>' . get_string('offline_training', 'block_course_status_tracker') .'</h2>';
        echo $title;
        $form->display();
        echo "</div>";
     }
}

}

/* Student Pages */
else{
if ($viewpage == 1) {
    $form = new course_status_form();
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    $table = $form->display_report();
    if ($table) {
        echo "<div id='prints'>";
        // $title = '<center><table width="80%" style="background-color:#F3F3F3;"><tr><td><center><h2>' . get_string('report_coursecompletion', 'block_course_status_tracker') . '</h2></center></td></tr></tr><table></center>';
        $title = '<h2>' . get_string('report_coursecompletion', 'block_course_status_tracker') . '</h2>';
        $title.=user_details($USER->id);
        $a = html_writer::table($table);
        echo $title;
        echo "<br/>".$a;
        echo "</div>";
    }
} else if ($viewpage == 2) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    // $title = '<center><table width="80%" style="background-color:#F3F3F3;"><tr><td><center><h2>' . get_string('report_courseenrollment', 'block_course_status_tracker') . '</h2></center></td></tr></tr><table></center>';
    $title = '<h2>' . get_string('report_courseenrollment', 'block_course_status_tracker') . '</h2>';
    $title.=user_details($USER->id);
    echo $title;
    $a = html_writer::table(user_enrolled_courses_report($USER->id));
    echo "<br/>".$a;
    echo "</div>";
} else if ($viewpage == 3) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    $title = '<h2>' . get_string('report_courseundefined', 'block_course_status_tracker') . '</h2>';
    $title.=user_details($USER->id);
    echo $title;
	echo "<br/>".html_writer::table(user_undefined_courses_report($USER->id));
    echo "</div>";
} else if ($viewpage == 4) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    echo "<div id='prints'>";
    $title = '<h2>' . get_string('report_courseinprogress', 'block_course_status_tracker') . '</h2>';
    $title.=user_details($USER->id);
        echo $title;
	echo "<br/>".html_writer::table(user_inprogress_courses_report($USER->id));
    echo "</div>";
} 

// Course Details
else if ($viewpage == 5) {
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'/blocks/course_status_tracker/view.php?viewpage=1">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
        echo "<div id='prints'>";
        $title = '<h2>' . get_string('course_details', 'block_course_status_tracker') . '</h2>';
        echo $title;
       // echo user_has_role_assignment($USER->id,5);
        course_detail($USER->id);
        echo "</div>";
}

// Accomplishment 
else if ($viewpage == 6) {
    $form = new accomplishment_form();
    //<!-- Back Button-->
    echo '<a href="'.$CFG->wwwroot.'">';
    echo '<input type="button" class="button" value="Back" >';
    echo '</a>';
    
    $table = $form->display_report();
    if ($table) {
        echo "<div id='prints'>";
        $title = '<h2>' . get_string('accomplishment', 'block_course_status_tracker') . '</h2>';
        $title.=user_details($USER->id);
        $a = html_writer::table($table);
        echo $title;
        echo '<div class="info">';
        if (user_instruction($USER->id) !=FALSE){
        echo '<p><strong>' .get_string('instruction', 'block_course_status_tracker') .': </strong>'. user_instruction($USER->id) .'</p>';}
        else{echo '<p><strong>' .get_string('instruction', 'block_course_status_tracker') .': </strong> ' .get_string('no_instruction', 'block_course_status_tracker') .'</p>';}
         
        echo '</div>';
        echo "<br/>".$a;
        echo "</div>";
       
    }
 }
}

echo $OUTPUT->footer();
