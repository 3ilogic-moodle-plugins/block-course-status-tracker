<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course
 * completion criteria named 'grade' based on login user.
 *
 * @package    block_course_status_tracker
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once($CFG->dirroot.'/lib/deprecatedlib.php');
require_once($CFG->dirroot.'/blocks/course_status_tracker/lib.php');
/**
 * This class shows the content in block through calling lib.php function.
 */
class block_course_status_tracker extends block_base {

    public function init() {
        $this->title = get_string('course_status_tracker', 'block_course_status_tracker');
    }

    /**
     * Where to add the block
     *
     * @return boolean
     * */
    public function applicable_formats() {
        return array('all' => true);
    }

    /**
     * Gets the contents of the block (course view)
     *
     * @return object An object with the contents
     * */
    public function isguestuser($user = null) {
        return false;
    }

    public function get_content() {
        global $CFG, $OUTPUT, $USER, $DB;
        if ($this->content !== null) {
            return $this->content;
        }
        $this->content = new stdClass;
        if ($CFG->enablecompletion) {
            // Enrolled courses.
            // $enrolled_courses=user_enrolled_courses($USER->id);
            $count_course = 0;
            $courses = enrol_get_users_courses($USER->id, false, 'id, shortname, showgrades');
            if ($courses) {
                foreach ($courses as $course) {
                    $count_course+=1;
                }
            }
            $enrolled_courses = count($courses);
            // End enrolled courses.
            // Completed courses.
            // $count_complete_courses=count_complete_course($USER->id);
//            $total_courses = $DB->get_record_sql('SELECT count(course) as total_course FROM {course_completion_crit_compl}  WHERE  userid = ? AND gradefinal <> "null"', array($USER->id));
//           
//            //$total_courses =  $DB->count_records_sql('SELECT DISTINCT(course) as total_course FROM {course_completion_crit_compl}  WHERE userid = ?', array($USER->id)); 
//            
//            $total_courses = $total_courses->total_course;
//            $count_complete_courses = $total_courses;
          //  echo $total_courses;
        // End completed courses.
            // Course criteria not set.
            // $course_criteria_not_set=count_course_criteria($USER->id);
//            $count = 0;
//            $courses = enrol_get_users_courses($USER->id, false, 'id, shortname, showgrades');
//            if ($courses) { 
//                $course_criteria_ns = array();
//                static $undefined_courses;
//                foreach ($courses as $course) {
//                    $exist = $DB->record_exists('course_completion_criteria', array('course' => $course->id));
//                    if (!$exist) {
//                        $count++;
//                        $course_criteria_ns[] = $course->id;
//                        $undefined_courses .= $course->id . ",";
//                    }
//                }
//            }
//         $course_criteria_not_set = $count;
         $course_criteria_not_set = count(undefinedcourses($courses));    
         $count_complete_courses =   count(copmletion($courses));
         $count_inprogress_courses =  count(inprogress($courses));
            
            // End course criteria.
            // Course inprogress
           // $count_inprogress_courses = abs(($enrolled_courses) - ($count_complete_courses + $course_criteria_not_set));
           if ($enrolled_courses > 0) {
                $link_enrolled_courses = "<u><a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=2'>" .
                        $enrolled_courses . "</a></u>";
            } else {
                $link_enrolled_courses = $enrolled_courses;
            }
            if ($count_complete_courses > 0) {
                $link_count_complete_courses = "<u><a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=1'>" .
                        $count_complete_courses . "</a></u>";
            } else {
                $link_count_complete_courses = $count_complete_courses;
            }

            if ($course_criteria_not_set > 0) {
                $link_course_criteria_not_set = "<u><a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=3'>" .
                        $course_criteria_not_set . "</a></u>";
            } else {
                $link_course_criteria_not_set = $course_criteria_not_set;
            }

            if ($count_inprogress_courses > 0) {
                $link_count_inprogress_courses = "<u><a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=4'>" .
                        $count_inprogress_courses . "</a></u>";
            } else {
                $link_count_inprogress_courses = $count_inprogress_courses;
            }
            
            $total_accomplishment = $DB->get_record_sql('SELECT count(bi.id) as b_total
                    FROM {badge} as b  ,{badge_issued} as bi , {badge_criteria} as bcri where bi.userid = ?  
                    AND b.id = bi.badgeid AND b.id = bcri.badgeid AND bi.visible = 1 AND bcri.criteriatype != 0', array($USER->id));
             
            $total_offline_accomplishment = $DB->get_record_sql('SELECT count(id) as accomp FROM {course_status_tracker}  WHERE u_name = ? ', array($USER->id));
            $total_accomplishment = $total_accomplishment->b_total + $total_offline_accomplishment->accomp;
            
            
            $total_offline_training =  $DB->get_record_sql('SELECT count(id) as accomp FROM {course_status_tracker} ', array(null));
            $total_training = $total_offline_training->accomp;
            
            if ($total_accomplishment > 0) {
                $link_accomplishment = "<u><a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=6'>" .
                        $total_accomplishment . "</a></u>";
            } else {
                $link_accomplishment = $total_accomplishment;
            }
            
            
            //Offline Training
             if ($total_training > 0) {
                $link_training = "<u><a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=8'>" .
                         $total_training . "</a></u>";
            } else {
                $link_training =  $total_training;
            }
            
            $this->content->text = '';
            $this->content->text .= get_string('enrolled_courses', 'block_course_status_tracker') . " :	<b>" . $link_enrolled_courses . "</b><br>";
            $this->content->text .= get_string('completed_courses', 'block_course_status_tracker') . " : <b>" . $link_count_complete_courses . "</b><br>";
            $this->content->text .= get_string('inprogress_courses', 'block_course_status_tracker') . " : <b>" . $link_count_inprogress_courses . "</b><br>";
            $this->content->text .= get_string('undefined_coursecriteria', 'block_course_status_tracker') . " : <b>" . $link_course_criteria_not_set . "</b><br>";
            $this->content->text .= get_string('accomplishment', 'block_course_status_tracker') ." :<b> " . $link_accomplishment . "</b><br>";
           
           //if(is_siteadmin()){
           if(user_has_role_assignment($USER->id,5) != 1){
             $this->content->text .= "<a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=7'>" .get_string('add_offline_training', 'block_course_status_tracker'). "</a><br>";
             $this->content->text .= get_string('view_offline_training', 'block_course_status_tracker') ." :<b> " . $link_training . "</b><br>";
             //Add Instruction Block Link
             $this->content->text .= "<a href='" . $CFG->wwwroot . "/blocks/course_status_tracker/view.php?viewpage=10'>" .get_string('add_instruction', 'block_course_status_tracker'). "</a><br>";
           }
            
        } else {
            $this->content->text .= get_string('coursecompletion_setting', 'block_course_status_tracker');
        }
        return $this->content;
    }

}
