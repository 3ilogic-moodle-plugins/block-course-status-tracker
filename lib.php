<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!--<link rel="stylesheet" type="text/css" href="http://localhost/moodle-3.2/blocks/course_status_tracker/course-custom.css">-->

<?php 
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

global $CFG;
require_once("{$CFG->libdir}/completionlib.php");
require_once($CFG->dirroot.'/completion/completion_completion.php');
/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course
 * completi  on criteria named 'grade' based on login user.
 *
 * @package    block_course_status_tracker
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


// Return array of undefinded courses/ course completion tracking not enable.
function undefinedcourses($courses) {
    global $CFG;
    $undefinecourses = array();
    foreach ($courses as $course) {
        $info = new completion_info($course);
        if (!$info->is_enabled()) {
            $undefinecourses[] = $course;
        }
    }
    return  $undefinecourses;
}


// Return array of complteded course
// Course copmletion ------------------------//
function copmletion($courses) {
    global $CFG, $USER;
    $coursecompleted = array();
    foreach ($courses as $course) {
        $info = new completion_info($course);
        $completion = $info->is_course_complete($USER->id);
        if ($completion) {
            $coursecompleted[] = $course;
        }
    }
    return $coursecompleted;
}

// Return array of inprogress courses
function inprogress($courses) {
    global $CFG, $USER;
    $inprogress = array();
    foreach ($courses as $course) {
        $info = new completion_info($course);
        if ($info->is_enabled()) {

            $completion = $info->is_course_complete($USER->id);
            if (!$completion) {
                $inprogress[] = $course;
            }
        }
    } return $inprogress;
}




function grade($course , $userid){
    global $DB;
     $sql = "SELECT course, gradefinal, timecompleted as dates FROM {course_completion_crit_compl} where userid = " . $userid. " AND course = ".$course ;
     $rs = $DB->get_record_sql($sql, array());
     return $rs->gradefinal;
     
}

function dates($course , $userid){
    global $DB;
     $sql = "SELECT course, gradefinal, timecompleted as dates FROM {course_completion_crit_compl} where userid = " . $userid. " AND course = ".$course ;
     $rs = $DB->get_record_sql($sql, array());
     return $rs->dates;
     
}




function block_course_status_tracker_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_MODULE) {
        return false; 
    }
 
    // Make sure the filearea is one of those used by the plugin.
    if ($filearea !== 'expectedfilearea' && $filearea !== 'anotherexpectedfilearea') {
        return false;
    }
 
    // Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).
    require_login($course, true, $cm);
 
    // Check the relevant capabilities - these may vary depending on the filearea being accessed.
    if (!has_capability('block/course_status_tracker_pluginfile:view', $context)) {
        return false;
    }
 
    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.
 
    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.
 
    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }
 
    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'block_course_status_tracker_pluginfile', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }
 
    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering. 
    // From Moodle 2.3, use send_stored_file instead.
    send_file($file, 86400, 0, $forcedownload, $options);
}


/*
function block_course_status_tracker_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $DB;
    if ($context->contextlevel != CONTEXT_BLOCK) {
        return false;
    }
    require_login();
    if ($filearea != 'attachment') {
        return false;
    }
    $itemid = (int)array_shift($args);
    if ($itemid != 0) {
        return false;
    }
    $fs = get_file_storage();
    $filename = array_pop($args);
    if (empty($args)) {
        $filepath = '/';
    } else {
        $filepath = '/'.implode('/', $args).'/';
    }
    $file = $fs->get_file($context->id, 'block_course_status_tracker', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false;
    }
    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!
}

*/
/**
 * This function return category id based on course id.
 *
 * @param int   $id course id
 * @return String category id.
 */
function get_category_id($course) {
    global $DB;

    $category_id_sql = "SELECT category FROM {course} where id = " . $course;
    $category_id_rs = $DB->get_record_sql($category_id_sql, array());
    if ($DB->record_exists_sql($category_id_sql, array())) {
        return $category_id_rs->category;
    }
}



/**
 * This function return context id.
 *
 * @param int   $id course id
 * @return String category id.
 */
function get_context_id() {
    global $DB;

    $sql = "SELECT c.id as context FROM {folder} as f, {course_modules} as cm , {context} as c  where f.id = cm.instance AND cm.id = c.instanceid AND cm.course = 1 AND c.contextlevel =70 AND   f.name = 'certificates' ";
    $result = $DB->get_record_sql($sql, array());
    
    if ($DB->record_exists_sql($sql, array())) {
        return $result->context;
    }
}


//course status tracker table insertion

function file_retreive_insert($fromform, $certificate) {
    global $DB, $USER;
    static $count;
    $records = new stdClass();
    $records->t_name = $fromform->t_name;
    $records->method = $fromform->method;
    $records->t_body = $fromform->t_body;
    $records->n_of_hours = $fromform->n_of_hours;
    $records->ivass = $fromform->ivass;
    $records->u_name = $fromform->u_name;
    $records->issuedate = $fromform->issuedate;
  
    // $records->certificate_name = $certificate;
    $DB->insert_record('course_status_tracker', $records);
  
}


//course status tracker table insertion

function file_retreive_update($fromform, $certificate) {

    global $DB, $USER;
    static $count;
    $records = new stdClass();
    $records->t_name = $fromform->t_name;
    $records->method = $fromform->method;
    $records->u_name = $fromform->u_name;
    $records->issuedate = $fromform->issuedate;
   // $records->certificate_name = $certificate;
    $DB->update_record('course_status_tracker', $records);
}

/**
 * This function display list of inprogress courses based on login user.
 *
 * @param int   $id user id
 * @return Table table.
 */
function user_inprogress_courses_report($userid) {
    global $CFG, $DB, $USER;

    $courses = enrol_get_users_courses($userid, false, 'id, shortname, showgrades');
    if ($courses) {
        $undefined_courses = '';
        $enroll_courses = '';
        foreach ($courses as $course) {
            $exist = $DB->record_exists('course_completion_criteria', array('course' => $course->id));
            $enroll_courses .= $course->id . ",";
            if (!$exist) {
                $undefined_courses .= $course->id . ",";
            }
        }
    }
      
    

     
       
// echo 'Total progress course: '. count($progress);
      
    //  $comp = copmletion($courses);
      //echo 'Total completed course: '. count($comp);
      
      echo '<pre>';
      // print_r($comp);
       echo '</pre>';

    
    $complete_course_sql = "SELECT course FROM {course_completion_crit_compl} where userid = " . $userid;
    $complete_courses_array = array();
    $complete_course_rs = $DB->get_records_sql($complete_course_sql, array());
    if ($DB->record_exists_sql($complete_course_sql, array())) {
        $complete_courses = '';
        foreach ($complete_course_rs as $complete_course_log) {
            $complete_courses .= $complete_course_log->course . ",";
			$complete_courses_array[] = $complete_course_log->course;
        }
    }
    
    
    $complete_courses = 0;
    $enrolled_courses = rtrim($enroll_courses, ',');
    $comp_undefined_courses = rtrim($undefined_courses, ',') . "," . rtrim($complete_courses, ',');


    $explode_enrolled_courses = explode(',', $enrolled_courses);
    $enrolled_courses_array = array();
    foreach ($explode_enrolled_courses as $explode_enroll) {
        $enrolled_courses_array[] = $explode_enroll;
    }


    $explode_comp_undefined_courses = explode(',', $comp_undefined_courses);
    $comp_undefined_courses_array = array();
    foreach ($explode_comp_undefined_courses as $explode_comp_undefined_courses) {
        $comp_undefined_courses_array[] = $explode_comp_undefined_courses;
    }


    //$inprogress_courses = array_diff($enrolled_courses_array, $comp_undefined_courses_array, $complete_courses_array);

  //  print_r($inprogress_courses);
    $inprogress_courses =  inprogress($courses);

        $params = array(
                'userid' => $USER->id,
                'course' => $course->id
            );
        
            
           // echo get_activity($USER->id);
            
            
    $table = new html_table();
    $table->attributes = array('class' => 'display');
    $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('module', 'block_course_status_tracker'), get_string('course_name', 'block_course_status_tracker'), get_string('activity_completion', 'block_course_status_tracker'));
    $table->align = array('center', 'left', 'left', 'center');
    $table->data = array();
    $i = 0;
         


	//print_r($inprogress_courses);

if (!empty($inprogress_courses)) {

    foreach ($inprogress_courses as $course) {
      
        $row = array();
        $row[] = ++$i;
        $row[] = module_name(get_category_id($course->id));
        $row[] = "<a href=" . $CFG->wwwroot . "/course/view.php?id=" . $course->id . ">" . course_name($course->id) . "</a>";
        //$row[] = get_attempted_activities($course, $userid). " / " .get_total_activities($course);
        $row[] = get_activity($USER->id,$course->id);
        //$row[] =$course;
        $table->data[] = $row;
    }
}

else
{
//$row[] = array();
$row[] = "";
$row[] = "";
$row[] = get_string('no_data', 'block_course_status_tracker');
$row[] = "";
$table->data[] = $row;


}

    return $table;
}

function get_activity($userid,$courseid)
{
    global $DB;
        //$courseid = 2;
          
        $courses = $DB->get_record('course', array('id'=>$courseid)); 
        $info = new completion_info($courses);
       // Load criteria to display.
        $completions = $info->get_completions($userid);
                // Generate markup for criteria statuses.
            $data = '';

            // For aggregating activity completion.
            $activities = array();
            $activities_complete = 0;

            // For aggregating course prerequisites.
            $prerequisites = array();
            $prerequisites_complete = 0;

            // Flag to set if current completion data is inconsistent with what is stored in the database.
            $pending_update = false;
                // Loop through course criteria.
            foreach ($completions as $completion) {
                $criteria = $completion->get_criteria();
                $complete = $completion->is_complete();

                if (!$pending_update && $criteria->is_pending($completion)) {
                    $pending_update = true;
                }

                // Activities are a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                    $activities[$criteria->moduleinstance] = $complete;

                    if ($complete) {
                        $activities_complete++;
                    }

                    continue;
                }

                // Prerequisites are also a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                    $prerequisites[$criteria->courseinstance] = $complete;

                    if ($complete) {
                        $prerequisites_complete++;
                    }

                    continue;
                }

            }
            
            if(count($activities) != 0){
            return $activities_complete." / ".count($activities);
            }
            else{
            return " - ";}
  
}
/**
 *  Get total activities in course
 * 
 */

function get_total_activities($cid)
{
   global $CFG, $DB;
   $total_activities = $DB->get_record_sql('SELECT count(cm.id) as activity FROM {modules} as m, {course_modules} as cm WHERE m.id = cm.module AND cm.course = ? AND m.id <> 3 AND m.id <> 6 AND m.id <> 8 AND m.id <> 11 AND m.id <> 12 AND m.id <> 15 AND m.id <> 20 ', array($cid));
  $total_activities = $total_activities->activity;
   return $total_activities;
}


/**
 *  Get Manually attempted activities in course
 * 
 */

function get_attempted_activities($cid, $userid)
{
   global $CFG, $DB;
   $total_activities = $DB->get_record_sql('SELECT count(cmc.id) as attempt FROM {course_modules_completion} as cmc, {modules} as m, {course_modules} as cm WHERE cm.id = cmc.coursemoduleid AND m.id = cm.module AND cm.course = ? AND userid = ? AND m.id <> 3 AND m.id <> 6 AND m.id <> 8 AND m.id <> 11 AND m.id <> 12 AND m.id <> 15 AND m.id <> 20', array($cid,$userid));
   $total_activities = $total_activities->attempt;
   return $total_activities;
    
}


/**
 * This function display list of undefined courses based on login user.
 *
 * @param int   $id user id
 * @return Table table.
 */
function user_undefined_courses_report($userid) {
    global $CFG, $DB;

    $courses = enrol_get_users_courses($userid, false, 'id, shortname, showgrades');
    
   // $incomp = undefinedcourses($courses);
    //echo count($incomp);
    
    if ($courses) {
        $table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('module', 'block_course_status_tracker'), get_string('course_name', 'block_course_status_tracker'));

        $table->align = array('center', 'left', 'left');
        $table->data = array();
        $i = 0;

        $course_criteria_ns = array();
        
        static $undefined_courses;
       
        foreach ($courses as $course) {
            $exist = $DB->record_exists('course_completion_criteria', array('course' => $course->id));
            if (!$exist) {
                $row = array();
                $row[] = ++$i;
                $row[] = module_name($course->category);
                $row[] = "<a href=" . $CFG->wwwroot . "/course/view.php?id=" . $course->id . ">" . course_name($course->id) . "</a>";
                $table->data[] = $row;
            }
            else{
            
                $row[] = "";
                $row[] = get_string('no_data', 'block_course_status_tracker');
                $row[] = "";
                $table->data[] = $row;
                break;
        }  
        }
        
        
        
        
        
    }
    return $table;
}

/**
 * This function count completed course based on login user.
 *
 * @param int   $id user id
 * @return String total course id.
 */
function count_complete_course($userid) {
    global $DB;
    $total_courses = $DB->get_record_sql('SELECT count(course) as total_course FROM {course_completion_crit_compl} WHERE userid = ?', array($userid));
    $total_courses = $total_courses->total_course;
    return $total_courses;
}

/**
 * This function display list of enrolled courses based on login user.
 *
 * @param int   $id user id
 * @return Table table.
 */
function user_enrolled_courses($userid) {
    global $CFG;
    $count_course = 0;
    $courses = enrol_get_users_courses($userid, false, 'id, shortname, showgrades');
    if ($courses) {
        foreach ($courses as $course) {
            $count_course+=1;
        }
    }
    return $count_course;
}

/**
 * This function returns total undefined courses based on login user.
 *
 * @param int   $id user id
 * @return String count number.
 */
function count_course_criteria($userid) {
    global $DB;
    $count = 0;
    $courses = enrol_get_users_courses($userid, false, 'id, shortname, showgrades');
    if ($courses) {
        $course_criteria_ns = array();
        foreach ($courses as $course) {
            $exist = $DB->record_exists('course_completion_criteria', array('course' => $course->id));
            if (!$exist) {
                $count++;
                $course_criteria_ns[] = $course->id;
            }
        }
    }
    return $count;
}

/**
 * This function return course category name based on course id.
 *
 * @param int   $id course id
 * @return String category name.
 */
function module_name($id) {
    global $DB;
    $module = $DB->get_record_sql('SELECT name FROM {course_categories}  WHERE id = ?', array($id));
    $module = format_string($module->name);
    return $module;
}




function get_user_certificate($id){
    global $DB;
 
    $module = $DB->get_record_sql('SELECT id FROM {course_modules} WHERE course = ? AND module =?', array($id, '23'));
//   // $module = format_string($module->name);
    return $module->id;
    
}

/** get course module **/

function get_course_module_id($course){
    global $DB;
    $module = 'SELECT cm.id as id FROM {course_modules} as cm , {modules} as m WHERE cm.module = m.id AND cm.course = ? AND m.name =?';
    if( $DB->record_exists_sql($module, array($course, 'iomadcertificate') ))
    {
        $result = $DB->get_record_sql($module, array($course, 'iomadcertificate'));
        return $result->id;
    }
    // $module = format_string($module->name);
    else{
        return NULL;
    }
   
    
} 


function get_manual_assign($userid , $badgeid){
    global $DB;
    $sql = "SELECT * FROM {badge_manual_award} WHERE badgeid = ? AND recipientid = ?";
    if($DB->record_exists_sql($sql, array($badgeid,$userid)))
    {
        return TRUE;
    }
    else{
        return FALSE;
    }
        
    
    
}

/**
 * This function returns course name based on course id.
 *
 * @param int   $id course id
 * @return String course name.
 */
function course_name($id) {
    global $DB;
    $course = $DB->get_record_sql('SELECT fullname  FROM {course} WHERE id = ?', array($id));
    $course = format_string($course->fullname);
    //$course = $course . ' ' . get_string('course', 'block_course_status_tracker');
    
    return $course;
}

/* User Instruction */
function user_instruction($id) {
    global $OUTPUT, $DB;
    $result = $DB->get_record_sql('SELECT instruction FROM {block_course_status_tracker} WHERE u_name = ?', array($id));
    if ($result != NULL){
    return $result->instruction; }
    else {
    return false; 
    }
}



/**
 * This function returns user details including user profile picture, name, department and joining date based on login user.
 *
 * @param int   $id user id
 * @return Table table.
 */
function user_details($id) {
    global $OUTPUT, $DB;
    // $user = new stdClass();
    $user = $DB->get_record('user', array('id' => $id));
    //$user->id = $id; // User Id.

    $user->picture = $OUTPUT->user_picture($user, array('size' => 100));
    // Fetch Data.
    $result = $DB->get_record_sql('SELECT concat(firstname," ",lastname) as name,institution, department, timecreated as date  FROM {user} WHERE id = ?', array($id));

    if ($result->date != '0') {
        $date = userdate($result->date, get_string('strftimedate', 'core_langconfig'));
    } else {
        $date = "-";
    }

    $table = '<table width="80%"><tr><td width="20%" style="vertical-align:middle;" rowspan="5">' . $user->picture . '</td></tr>
           <tr><td width="20%">' . get_string('name', 'block_course_status_tracker') . '</td><td>' . $result->name . '</td></tr>';

    $check_designatino_field = report_get_custome_field($id, "Designation"); // Custom Field name for designation is "Designation".
    if ($check_designatino_field != 0) {
        $table .='<tr><td>' . get_string('job_title', 'block_course_status_tracker') . '</td><td>' . format_string($check_designatino_field) . '</td></tr>';
    }
    $table .='<tr><td>' . get_string('department', 'block_course_status_tracker') . '</td><td>' . format_string($result->department) . '</td></tr>
             <tr><td>' . get_string('joining_date', 'block_course_status_tracker') . '</td><td>' . $date . '</td></tr>
              <tr><td>' . get_string('institution', 'block_course_status_tracker') . '</td><td>' . $result->institution . '</td></tr>    
             </table>';
    return $table;
}

/**
 * This function returns custom field based on login user and field name.
 *
 * @param int   $id user id, $text field name
 * @return String filed data.
 */
function report_get_custome_field($userid, $text) {
    global $DB;
    $result = $DB->get_record_sql('SELECT table2.data as fieldvalue  FROM {user_info_field} as table1  join  {user_info_data} as table2
                                   on table1.id=table2.fieldid where table2.userid=? AND table1.name=?', array($userid, $text));

    $fieldvalue = $result['fieldvalue'];
    if (empty($fieldvalue)) {
        return "0";
    } else {
        return format_string($result->fieldvalue);
    }
}

/**
 * This function display list of enroled courses based on login user.
 *
 * @param int   $id user id
 * @return Table table.
 */
function user_enrolled_courses_report($userid) {
    global $CFG;
    $count_course = 0;
    $courses = enrol_get_users_courses($userid, false, 'id, shortname, showgrades');
    if ($courses) {
        $table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('module', 'block_course_status_tracker'), get_string('course_name', 'block_course_status_tracker'));

        $table->align = array('center', 'left', 'left');
        $table->data = array();
        $i = 0;
        foreach ($courses as $course) {
            $row = array();
            $row[] = ++$i;
            $row[] = module_name($course->category);
            $row[] = "<a href=" . $CFG->wwwroot . "/course/view.php?id=" . $course->id . ">" . course_name($course->id) . "</a>";
            $table->data[] = $row;
        }
    }
    return $table;
}

/** Return certificate exist or not  **/
function get_certificate_info(){
    global $DB, $OUTPUT, $CFG, $USER;
    $userid = $USER->id;
    
    $sql = "Select * from {customcert_issues} where userid =? ";
    $query = $DB->record_exists_sql($sql, array($userid));
    if($query == NULL)
    {
        return FALSE;
        
    }
    
    else
    {
        return TRUE;
    }
    
    
}

// viewpage = 5
// course detail/description
function course_detail($userid)  {

 global $DB, $OUTPUT, $CFG, $USER;
        $userid = $USER->id;
        // Page parameters.
        $cid = optional_param('cid', 0, PARAM_INT);
        //echo $cid;
        
        
        $sql = "Select * FROM {course} WHERE id =$cid"; 
        $query = $DB->get_record_sql($sql, array());
        ?>
        <table id="example" class="cell-border" style="width:100%">
        <tr>
            <th><?php echo get_string('course_name', 'block_course_status_tracker'); ?></th>
            <td><?php echo $query->fullname ?></td>
        <tr>
        <tr>
            <th><?php echo get_string('course_category', 'block_course_status_tracker'); ?></th>
            <td><?php echo module_name(get_category_id($cid)) ?></td>
        </tr>
        <tr>
            <th><?php echo get_string('course_start_date', 'block_course_status_tracker'); ?></th>
            <td><?php if ($query->startdate == 0 ){ echo " - "; } else { echo userdate($query->startdate);} ?></td>
        </tr>
        
        <tr>
            <th><?php echo get_string('course_end_date', 'block_course_status_tracker'); ?></th>
            <td><?php if ($query->enddate == 0 ){ echo " - ";} else { echo userdate($query->enddate);  } ?></td>
        </tr>
        
        <tr>
            <th><?php echo get_string('course_description', 'block_course_status_tracker'); ?></th>
            <td><?php if ($query->summary!= NULL){echo $query->summary;} else { echo '-';} echo "\n"; 
            
            /*echo "<b>Duration Time: </b> "; 
            if(user_course_duration($userid,$cid) != FALSE){
                echo secToHR(user_course_duration($userid,$cid));
            }
            else{
                echo "0 min";
            }*/
?> </td>
        </tr>

        <tr>
            <th><?php echo get_string('certificate', 'block_course_status_tracker'); ?></th>
            <?php if (get_course_module_id($cid) != NULL){?>
            <td><a href="<?php echo $CFG->wwwroot; ?>/mod/iomadcertificate/view.php?id=<?php echo get_course_module_id($cid); ?>"><img src="<?php echo $CFG->wwwroot; ?>/blocks/course_status_tracker/pix/icon.gif" ></img></a></td>
            <?php } else { ?>  
            <td> - </td>
            <?php } 
       
            ?>
        </tr>
        </tbody>
    </table>
        
    <?php    
}

function secToHR($seconds) {
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60);
  $seconds = $seconds % 60;
  return "$hours h, $minutes min";
}


function user_course_duration($userid,$cid){
    
    global $DB; 
    $sql = "SELECT DISTINCT(duration) as time FROM {attendanceregister_aggregate} arg "
            . "INNER JOIN {attendanceregister} ar "
            . "ON  arg.register = ar.id "
            . "WHERE ar.course = ? AND arg.userid=? ";
    
    if($DB->record_exists_sql($sql, array($cid, $userid) ))
    {
        $result = $DB->get_record_sql($sql, array($cid,$userid));
        return format_string(floor($result->time));
    }
    else{
        return FALSE;
    }

    
    
    
//    $sql = "SELECT DISTINCT(arg.duration) as time FROM {attendanceregister_aggregate} arg INNER JOIN {attendanceregister} ar ON  arg.register = ar.id WHERE ar.course = ? AND arg.userid=?";
//    $rs = $DB->get_record_sql($sql, array($cid,$userid));
//    echo $rs->time;
    
   // $result = $DB->get_record_sql('SELECT DISTINCT(duration) as time FROM {attendanceregister_aggregate} arg INNER JOIN {attendanceregister} ar ON  arg.register = ar.id WHERE ar.course = ? AND arg.userid=? ', array($cid,$userid));
   // return $result ."Hi";
    //return $result;
    /*$sql1 = "SELECT DISTINCT(duration) as time FROM {attendanceregister_aggregate} arg '
            . ' INNER JOIN {attendanceregister} ar '
            . ' ON  arg.register = ar.id '
            . ' WHERE ar.course = ? AND arg.userid=? ";
    
   
   // $rs = $DB->get_record_sql($sql1, array($cid,$userid));
    if ($DB->record_exists_sql($sql1, array($cid,$userid))) {
        echo "TRUE";
    }
    //floor($rs->time/60)
    else{
     echo "FALSE";   
    
    }*/
    
    
}


/**
 * This function display list of total offline training.
 *
 * @param int   $id user id
 * @return Table table.
 */
function user_offline_training_report() {
    
    global $DB, $OUTPUT, $CFG, $USER;
    $count_course = 0;
    $training = $DB->get_records_sql('SELECT * FROM {course_status_tracker} ', array(null));
            
    if ($training) {
        $table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('name', 'block_course_status_tracker'), get_string('training_name', 'block_course_status_tracker'), get_string('training_method', 'block_course_status_tracker'), get_string('training_body', 'block_course_status_tracker'),get_string('n_of_hours', 'block_course_status_tracker'),get_string('ivass', 'block_course_status_tracker'), get_string('edit', 'block_course_status_tracker'), get_string('remove', 'block_course_status_tracker'));
        $table->align = array('center', 'left', 'left');
        $table->data = array();
        $i = 0;
        foreach ($training as $tmp) {
            $row = array();
            $row[] = ++$i;
            $row[] =  get_user($tmp->u_name); 
            $row[] = $tmp->t_name;
            if ($tmp->method == 0){
            $row[] = get_string('face_to_face', 'block_course_status_tracker');}
            else if($tmp->method == 1){
            $row[] = get_string('blended', 'block_course_status_tracker');}
            else if($tmp->method == 2){
            $row[] = get_string('online_lms', 'block_course_status_tracker');}
            /* New fields*/
            $row[] = $tmp->t_body;
            $row[] = secToHR($tmp->n_of_hours);
            if ($tmp->ivass == 0){
            $row[] = get_string('yes', 'block_course_status_tracker');}
            else {
            $row[] = get_string('no', 'block_course_status_tracker');}
            /* End New fields*/
            $row[] = '<center><center><a title="' . get_string('edit') . '" href="' . $CFG->wwwroot . '/blocks/course_status_tracker/view.php?viewpage=9&edit=edit&id=' . $tmp->id. '"/>
                      <img alt="" src="' . $OUTPUT->pix_url('t/edit') . '" class="iconsmall" /></a></center>';
            $row[] = '<center><center><a title="' . get_string('delete') . '" href="' . $CFG->wwwroot . '/blocks/course_status_tracker/view.php?viewpage=9&rem=remove&id=' . $tmp->id. '"/>
                       <img alt="" src="' . $OUTPUT->pix_url('t/delete') . '" class="iconsmall"/></a></center>';
            $table->data[] = $row;
        }
    }


else{
	$table = new html_table();
        $table->attributes = array('class' => 'display');
        $table->head = array(get_string('s_no', 'block_course_status_tracker'), get_string('name', 'block_course_status_tracker'), get_string('training_name', 'block_course_status_tracker'), get_string('training_method', 'block_course_status_tracker'), get_string('edit', 'block_course_status_tracker'), get_string('remove', 'block_course_status_tracker'));
        $table->align = array('center', 'left', 'left');
        $table->data = array();
        $table->data[] = array('','',get_string('no_data', 'block_course_status_tracker'),'','','');
}


    return $table;
}


function get_user($id){
    global $CFG,$DB;
    $user = $DB->get_record_sql('SELECT concat(firstname," ",lastname) as name  FROM {user} where id = ? ', array($id));
    return $user->name;   
}


?>

<script>
    $(document).ready(function() {
    $('#example').DataTable(
    );
} );

</script>