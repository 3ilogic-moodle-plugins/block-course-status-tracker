<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block to display enrolled, completed, inprogress and undefined courses according to course completion criteria named 'grade' based on login user.
 *
 * @package    block_course_status_tracker
 * @copyright  3i Logic<lms@3ilogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */
defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Course status tracker';
$string['course_status'] = 'Course status tracker';
$string['course_status_tracker'] = 'Course status tracker';
$string['s_no'] = 'Serial <abbr title="number">No.</abbr>';
$string['course_name'] = 'Course Name';
$string['course_comp_date'] = 'Course completion date';
$string['grade'] = 'Grade';
$string['course_status_tracker:view'] = 'Course status view';
$string['course_status_tracker:addinstance'] = 'Add instance';
$string['enrolled_courses'] = 'Enrolled courses';
$string['completed_courses'] = 'Completed courses';
$string['inprogress_courses'] = 'Course in progress';
$string['accomplishment'] = 'Accomplishments';
$string['offline_training'] = 'Offline Trainings';
$string['add_offline_training'] = 'Add offline training';
$string['view_offline_training'] = 'View offline training';
$string['add_instruction'] = 'Add instruction';
$string['instruction'] = 'Instruction';
$string['validation_instruction'] = 'Please add instruction';
$string['no_instruction'] = 'No instruction yet !!';


$string['undefined_coursecriteria'] = 'Course without completion criteria';
$string['coursecompletion_setting'] = 'Course completion has not enable yet';
$string['job_title'] = 'Job Title';
$string['department'] = 'Department';
$string['joining_date'] = 'Joining Date';
$string['course'] = 'Course';
$string['report_coursecompletion'] = 'Course completion';
$string['report_courseenrollment'] = 'Course enrolment';
$string['report_courseundefined'] = 'Course without completion tracking';
$string['report_courseinprogress'] = 'Course In progress';
$string['module'] = 'Module';
$string['name'] = 'Name';
$string['certificate'] = 'Certificate';
$string['detail'] = 'Details';
$string['title'] = 'Title';
$string['issued_date'] = 'Issued Date';
$string['training_method'] = 'Training Method';
$string['training_name'] = 'Training Name';
$string['validation_training'] ='Please add training name';
$string['face_to_face'] = 'face-to-face';
$string['blended'] = 'Blended';
$string['online_lms'] = 'Online in other LMS';
$string['online'] = 'Online';
$string['no_data'] = 'No Data Found';


$string['edit'] = 'Edit';
$string['remove'] = 'Remove';
$string['activity_completion'] = 'Activities Completion';
$string['institution'] = 'Institution';


$string['course_details'] = 'Course Details';
$string['course_category'] = 'Course Category';
$string['course_start_date'] = 'Course Start Date';
$string['course_end_date'] = 'Course End Date';
$string['course_description'] = 'Course Description';


$string['training_body'] = 'Training Body';
$string['validation_training_body'] ='Please add training body';
$string['n_of_hours'] = 'No. of Certified Hours';
$string['certification_duration'] = 'Certification Duration';

$string['validation_certified_hours'] ='Please add number of certified hours';
$string['ivass'] = 'IVASS';
$string['yes'] = 'Yes';
$string['no'] = 'No';

$string['validation_name'] ='Please Select User';




